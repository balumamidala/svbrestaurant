package com.svbResturant;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SvbResturantApplication {

	public static void main(String[] args) {
		SpringApplication.run(SvbResturantApplication.class, args);
	}

}
