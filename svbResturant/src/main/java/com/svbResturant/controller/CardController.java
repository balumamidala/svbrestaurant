package com.svbResturant.controller;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.svbResturant.jdbc.CardJdbc;
import com.svbResturant.request.CardDetails;
import com.svbResturant.response.CardDetailsResponse;
import com.svbResturant.response.ItemDetailsResponse;

@RestController
public class CardController {
	
	@PostMapping("/card")
	public ItemDetailsResponse addCardDetails(@RequestBody CardDetails cardDetails) {
		
		CardJdbc details = new CardJdbc();
		boolean cardDetailsOutput = details.addCardDetails(cardDetails);
		ItemDetailsResponse itemResponse = new ItemDetailsResponse();
		if(cardDetailsOutput == true) {
			itemResponse.setStatus("sucess");
			itemResponse.setStatusText("record inserted sucessfully");
		}else {
			itemResponse.setStatus("failure");
			itemResponse.setStatusText("failed to insert records");
		}

		return itemResponse;
	}
	
@GetMapping("/getcard")
public CardDetailsResponse getCardDetails(CardDetails cardDetails) {
	CardJdbc cardJdbc = new CardJdbc();
	CardDetails cardDetailsOutput = cardJdbc.getCardDetails(cardDetails);
	
	CardDetailsResponse cardDetailsResponse = new CardDetailsResponse();
	if(cardDetailsOutput!=null) {
		cardDetailsResponse.setStatus("sucess");
		cardDetailsResponse.setStatusText("record inserted sucessfully");
		cardDetailsResponse.setCardType(cardDetailsOutput.getCardType());
		cardDetailsResponse.setCardNumber(cardDetailsOutput.getCardNumber());
		cardDetailsResponse.setCardHolderName(cardDetailsOutput.getCardHolderName());
	} else {
		cardDetailsResponse.setStatus("failure");
		cardDetailsResponse.setStatusText("failed to insert records");
	}

	return cardDetailsResponse;
		
	}
			
}

