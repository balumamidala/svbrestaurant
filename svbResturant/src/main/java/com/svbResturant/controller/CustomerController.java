package com.svbResturant.controller;

import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.svbResturant.jdbc.CustomerJdbc;
import com.svbResturant.request.LoginRequest;
import com.svbResturant.request.SignupRequest;
import com.svbResturant.response.LoginResponse;
import com.svbResturant.response.StatusResponse;

@CrossOrigin
@RestController
public class CustomerController {

	@PostMapping("/signup")
	public StatusResponse signupDetails(@RequestBody SignupRequest signupRequest) {

		CustomerJdbc customerJdbc = new CustomerJdbc();
		boolean signupResult = customerJdbc.insertCustomerDetails(signupRequest);
		StatusResponse statusResponse = new StatusResponse();
		if (signupResult == true) {
			statusResponse.setStatus("sucess");
			statusResponse.setStatusText("record inserted sucessfully");

		} else {
			statusResponse.setStatus("failure");
			statusResponse.setStatusText("failed to insert records");
		}

		return statusResponse;
	}

	@PostMapping("/login")
	public LoginResponse getLoginDetails(@RequestBody LoginRequest loginRequest) {

		CustomerJdbc customerJdbc = new CustomerJdbc();
		int customerId = customerJdbc.verifyCustomer(loginRequest);
		LoginResponse loginResponse = new LoginResponse();
		if (customerId != 0) {

			loginResponse.setStatus("success");
			loginResponse.setStatusText("record found");
			loginResponse.setCustomerId(customerId);

		} else {
			loginResponse.setStatus("failure");
			loginResponse.setStatusText("Record not found");
		}
		return loginResponse;
	}

}
