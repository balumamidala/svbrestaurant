package com.svbResturant.controller;

import java.util.ArrayList;

import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.svbResturant.jdbc.ItemsJdbc;
import com.svbResturant.request.AddItemCategoryDetailsRequest;
import com.svbResturant.request.ItemDetailsRequest;
import com.svbResturant.response.GetItemCategoryResponse;
import com.svbResturant.response.ItemCategoryResponse;
import com.svbResturant.response.ItemDetailsResponse;
import com.svbResturant.response.ItemResponse;
import com.svbResturant.response.StatusResponse;
@CrossOrigin
@RestController
public class ItemsController {

	@PostMapping("/items")
	public StatusResponse addItems(@RequestBody ItemDetailsRequest itemDetailsRequest) {

		ItemsJdbc itemsResult = new ItemsJdbc();
		boolean output = itemsResult.addItems(itemDetailsRequest);
		StatusResponse statusResponse = new StatusResponse();
		if (output == true) {
			statusResponse.setStatus("sucess");
			statusResponse.setStatusText("record inserted sucessfully");
		} else {
			statusResponse.setStatus("failure");
			statusResponse.setStatusText("failed to insert records");
		}

		return statusResponse;

	}

	@GetMapping("/item-category")
	public ItemCategoryResponse getItemCategory() {
		ItemCategoryResponse categoryResponse = new ItemCategoryResponse();
		ItemsJdbc itemsJdbc = new ItemsJdbc();
		ArrayList<GetItemCategoryResponse> itemCategoryList = itemsJdbc.getItems();
		if (itemCategoryList != null && !itemCategoryList.isEmpty()) {
			categoryResponse.setStatus("success");
			categoryResponse.setStatusText("record found");
			categoryResponse.setItemCategories(itemCategoryList);
		} else
			categoryResponse.setStatus("failure");
		categoryResponse.setStatusText("record not found");

		return categoryResponse;
	}

	@PostMapping("/item-category")
	public StatusResponse getItemProperties(@RequestBody AddItemCategoryDetailsRequest addItemCategoryDetailsRequest) {
		ItemsJdbc itemsJdbc = new ItemsJdbc();
		boolean itemsCategoryOutput = itemsJdbc.addItemCategoryDetails(addItemCategoryDetailsRequest);
		StatusResponse statusResponse = new StatusResponse();
		if (itemsCategoryOutput == true) {
			statusResponse.setStatus("success");
			statusResponse.setStatusText("records inserted");
		} else {
			statusResponse.setStatus("failed");
			statusResponse.setStatusText("failed to insert");
		}
		return statusResponse;
	}

	@GetMapping("/item")
	public ItemDetailsResponse getSelectedItems(@RequestParam("itemCategoryId") int itemCategoryId) {
		ItemsJdbc itemsJdbc = new ItemsJdbc();
		ArrayList<ItemResponse> itemsDetailsList = itemsJdbc.getSelectedItems(itemCategoryId);
		ItemDetailsResponse itemDetailsResponse = new ItemDetailsResponse();
		if (itemsDetailsList != null && !itemsDetailsList.isEmpty()) {
			itemDetailsResponse.setStatus("success");
			itemDetailsResponse.setItemDetails(itemsDetailsList);
		} else {
			itemDetailsResponse.setStatus("failed");
			itemDetailsResponse.setStatusText("try again");
		}
		return itemDetailsResponse;
	}
}
