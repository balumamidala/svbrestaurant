package com.svbResturant.controller;

import java.util.ArrayList;

import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.svbResturant.jdbc.OrderJdbc;
import com.svbResturant.request.OrderDetails;
import com.svbResturant.response.OrderDetailsResponse;
import com.svbResturant.response.OrderResponse;
import com.svbResturant.response.StatusResponse;
@CrossOrigin
@RestController
public class OrderController {
	@PostMapping("/order")
	public StatusResponse insertOrderDetails(@RequestBody OrderDetails orderDetails) {
		OrderJdbc order = new OrderJdbc();
		int orderId = order.insertOrderDetails(orderDetails);
		StatusResponse statusResponse = new StatusResponse();
		boolean ordersInserted = false;
		if (orderId != 0) {

			ordersInserted = order.insertOrderItems(orderDetails, orderId);

			if (ordersInserted == true) {
				statusResponse.setStatus("sucess");
				statusResponse.setStatusText("record inserted sucessfully");
			} else {
				statusResponse.setStatus("failure");
				statusResponse.setStatusText("failed to insert order items");
			}

		} else

		{
			statusResponse.setStatus("failed");
			statusResponse.setStatusText("failed to insert order Details");
		}
		return statusResponse;

	}

	@GetMapping("/order-history")
	public OrderDetailsResponse verifyData(@RequestParam("customerId") int customerId) {
		OrderJdbc idResult = new OrderJdbc();

		ArrayList<OrderResponse> itemDetails = idResult.orderHistory(customerId);

		OrderDetailsResponse orderResponse = new OrderDetailsResponse();
		if (itemDetails != null && !itemDetails.isEmpty()) {

			orderResponse.setStatus("success");
			orderResponse.setTotalOrders(itemDetails.size());
			orderResponse.setCustomerId(customerId);
			orderResponse.setOrderDetails(itemDetails);

		} else {
			orderResponse.setStatus("failed");
			orderResponse.setStatusText("try again");
		}
		return orderResponse;
	}

}
