package com.svbResturant.jdbc;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

import com.svbResturant.request.LoginRequest;
import com.svbResturant.request.SignupRequest;

public class CustomerJdbc {

	public boolean insertCustomerDetails(SignupRequest signupRequest) {
		boolean recordInserted = false;

		try {
			DbConnection dbConnection = new DbConnection();
			Connection connection = dbConnection.getConnection();

			String query = "insert into customer ( first_name,last_name,email,password,phone_number,address) values(?,?,?,?,?,?)";

			PreparedStatement statement = connection.prepareStatement(query);
			statement.setString(1, signupRequest.getFirstName());
			statement.setString(2, signupRequest.getLastName());
			statement.setString(3, signupRequest.getEmail());
			statement.setString(4, signupRequest.getPassword());
			statement.setString(5, signupRequest.getPhoneNumber());
			statement.setString(6, signupRequest.getAddress());

			int count = statement.executeUpdate();
			if (count == 1) {
				recordInserted = true;
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return recordInserted;

	}

	public int verifyCustomer(LoginRequest loginRequest) {

		int customerId = 0;

		try {
			DbConnection dbConnection = new DbConnection();
			Connection connection = dbConnection.getConnection();

			String query = "select * from customer where email= ? and password=?";
			PreparedStatement statement = connection.prepareStatement(query);

			statement.setString(1, loginRequest.getEmail());
			statement.setString(2, loginRequest.getPassword());

			ResultSet rs = statement.executeQuery();
			if (rs.next()) {

				customerId = rs.getInt("customer_id");

			}

		} catch (Exception e) {
			e.printStackTrace();
		}

		return customerId;
	}
}
