package com.svbResturant.jdbc;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;

import com.svbResturant.request.AddItemCategoryDetailsRequest;
import com.svbResturant.request.ItemDetailsRequest;
import com.svbResturant.response.GetItemCategoryResponse;
import com.svbResturant.response.ItemResponse;

public class ItemsJdbc {

	public boolean addItems(ItemDetailsRequest itemDetails) {
		boolean recordInserted = false;

		try {
			DbConnection dbConnection = new DbConnection();
			Connection connection = dbConnection.getConnection();

			String query = "insert into items (item_name ,item_price,item_type,item_description,item_category_id,item_image_name)"
					+ "values(?,?,?,?,?,?)";

			PreparedStatement statement = connection.prepareStatement(query);

			statement.setString(1, itemDetails.getItemName());
			statement.setDouble(2, itemDetails.getItemPrice());
			statement.setString(3, itemDetails.getItemType());
			statement.setString(4, itemDetails.getItemDescription());
			statement.setInt(5, itemDetails.getItemCategoryId());
			statement.setString(6, itemDetails.getItemImageName());

			int count = statement.executeUpdate();

			if (count == 1) {
				recordInserted = true;
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return recordInserted;
	}

	public ArrayList<GetItemCategoryResponse> getItems() {

		ArrayList<GetItemCategoryResponse> list = new ArrayList<>();

		try {
			DbConnection dbConnection = new DbConnection();
			Connection connection = dbConnection.getConnection();

			String query = "select * from item_category";

			PreparedStatement statement = connection.prepareStatement(query);

			ResultSet rs = statement.executeQuery();
			while (rs.next()) {

				int itemCategoryId = rs.getInt("item_category_id");
				String itemCategoryName = rs.getString("item_category_name");
				String itemCategoryImageName = rs.getString("item_category_image_name");

				GetItemCategoryResponse getItemCategoryResponse = new GetItemCategoryResponse();
				getItemCategoryResponse.setItemCategoryId(itemCategoryId);
				getItemCategoryResponse.setItemCategoryName(itemCategoryName);
				getItemCategoryResponse.setItemCategoryImageName(itemCategoryImageName);

				list.add(getItemCategoryResponse);

			}

		} catch (Exception e) {
			e.printStackTrace();
		}

		return list;
	}

	public boolean addItemCategoryDetails(AddItemCategoryDetailsRequest addItemCategoryDetailsRequest) {

		boolean recordInserted = false;

		try {

			DbConnection dbConnection = new DbConnection();
			Connection connection = dbConnection.getConnection();

			String query = "insert into item_category(item_category_name,item_category_image_name) values(?,?)";

			PreparedStatement statement = connection.prepareStatement(query);

			statement.setString(1, addItemCategoryDetailsRequest.getItemCategoryName());
			statement.setString(2, addItemCategoryDetailsRequest.getItemCategoryImageName());

			int count = statement.executeUpdate();

			if (count == 1) {
				recordInserted = true;
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return recordInserted;
	}

	public ArrayList<ItemResponse> getSelectedItems(int itemCategoryId) {

		ArrayList<ItemResponse> list = new ArrayList();
		try {
			DbConnection dbConnection = new DbConnection();
			Connection connection = dbConnection.getConnection();

			String query = "select * from items where item_category_id= ?";

			PreparedStatement statement = connection.prepareStatement(query);

			statement.setInt(1, itemCategoryId);

			ResultSet rs = statement.executeQuery();
			while (rs.next()) {

				int itemId = rs.getInt("item_id");
				String itemName = rs.getString("item_name");
				double itemPrice = rs.getDouble("item_price");
				String itemType = rs.getString("item_type");
				String itemImageName = rs.getString("item_image_name");

				ItemResponse itemDetails = new ItemResponse();

				itemDetails.setItemId(itemId);
				itemDetails.setItemName(itemName);
				itemDetails.setItemPrice(itemPrice);
				itemDetails.setItemType(itemType);
				itemDetails.setItemImageName(itemImageName);

				list.add(itemDetails);
			}

		} catch (Exception e) {
			e.printStackTrace();
		}

		return list;
	}
}
