package com.svbResturant.jdbc;

import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;

import com.svbResturant.request.OrderDetails;
import com.svbResturant.request.OrderItem;
import com.svbResturant.response.OrderItems;
import com.svbResturant.response.OrderResponse;

public class OrderJdbc {
		

	public int insertOrderDetails(OrderDetails orderDetails) {

		int orderId = 0;
		try {

			DbConnection dbConnection = new DbConnection();
			Connection connection = dbConnection.getConnection();

			Timestamp timestamp = getTime();
			String query = "insert into orders (customer_id,order_date_time) values (?,?)";

			PreparedStatement statement = connection.prepareStatement(query, Statement.RETURN_GENERATED_KEYS);

			statement.setInt(1, orderDetails.getCustomerId());
			statement.setTimestamp(2, timestamp);

			int count = statement.executeUpdate();
			if (count == 1) {
				ResultSet rs = statement.getGeneratedKeys();
				if (rs.next()) {
					orderId = rs.getInt(1);
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return orderId;

	}

	private Timestamp getTime() {
		Calendar calendar = Calendar.getInstance();
		Timestamp timeStamp = new Timestamp(calendar.getTimeInMillis());
		return timeStamp;
	}

	public boolean insertOrderItems(OrderDetails orderDetails, int orderId) {
		boolean recordInserted = false;
		try {

			DbConnection dbConnection = new DbConnection();
			Connection connection = dbConnection.getConnection();

			String query = "insert into order_item ( order_id,quantity,item_id) values (?,?,?)";

			PreparedStatement statement = connection.prepareStatement(query);
			ArrayList<OrderItem> orderResult = orderDetails.getOrderItems();
			for (OrderItem orderItem : orderResult) {

				statement.setInt(1, orderId);
				statement.setInt(2, orderItem.getQuantity());
				statement.setInt(3, orderItem.getItemId());
				statement.addBatch();
			}

			int[] count = statement.executeBatch();
			if (count.length >= 1) {
				recordInserted = true;
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return recordInserted;

	}
	
	public ArrayList<OrderResponse> orderHistory(int customerId) {

		HashMap<Integer, ArrayList<OrderItems>> orderItemsMap = new HashMap<>();
		ArrayList<OrderResponse> orderResponseList = new ArrayList<>();
		try {
			DbConnection dbConnection = new DbConnection();
			Connection connection = dbConnection.getConnection();

			String query = "select * from orders o " + "join order_item oi on o.order_id=oi.order_id "
					+ "join items i on oi.item_id = i.item_id " + " where customer_id= ?";

			PreparedStatement statement = connection.prepareStatement(query);

			statement.setInt(1, customerId);

			ResultSet rs = statement.executeQuery();
			while (rs.next()) {

				String itemName = rs.getString("item_name");
				double itemPrice = rs.getDouble("item_price");
				Date orderTime = rs.getDate("order_date_time");
				int orderId = rs.getInt("order_id");

				OrderItems orderItems = new OrderItems();
				orderItems.setItemName(itemName);
				orderItems.setItemPrice(itemPrice);
				orderItems.setOrderTime(orderTime);
				
				if(orderItemsMap.isEmpty() || orderItemsMap.get(orderId)==null) {
					ArrayList<OrderItems> orderItemsList = new ArrayList<>();
					orderItemsList.add(orderItems);
					orderItemsMap.put(orderId, orderItemsList);
				}else {
					for (int oid : orderItemsMap.keySet()) {
						if (oid == orderId) {
							ArrayList<OrderItems> mapItems = orderItemsMap.get(oid);
							mapItems.add(orderItems);
							orderItemsMap.put(oid, mapItems);
						} 
					}
				}

			}

			for (int oid : orderItemsMap.keySet()) {
				OrderResponse orderResponse = new OrderResponse();
				orderResponse.setOrderId(oid);
				orderResponse.setOrders(orderItemsMap.get(oid));
				orderResponseList.add(orderResponse);
			}

		} catch (Exception e) {
			e.printStackTrace();
		}

		return orderResponseList;
	}
}
