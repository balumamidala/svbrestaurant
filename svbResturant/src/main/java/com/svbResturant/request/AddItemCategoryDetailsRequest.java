package com.svbResturant.request;

public class AddItemCategoryDetailsRequest {
	
	private String itemCategoryName;
	private String itemCategoryImageName;
	
	
	public String getItemCategoryName() {
		return itemCategoryName;
	}
	public void setItemCategoryName(String itemCategoryName) {
		this.itemCategoryName = itemCategoryName;
	}
	public String getItemCategoryImageName() {
		return itemCategoryImageName;
	}
	public void setItemCategoryImageName(String itemCategoryImageName) {
		this.itemCategoryImageName = itemCategoryImageName;
	}
	
	

}
