package com.svbResturant.request;

public class ItemDetailsRequest {

	private String itemName;
	private String itemDescription;
	private double itemPrice;
	private int itemCategoryId;
	private String itemType;
	private String itemImageName;
	
	
	
	public String getItemName() {
		return itemName;
	}
	public void setItemName(String itemName) {
		this.itemName = itemName;
	}
	public String getItemDescription() {
		return itemDescription;
	}
	public void setItemDescription(String itemDescription) {
		this.itemDescription = itemDescription;
	}
	public double getItemPrice() {
		return itemPrice;
	}
	public void setItemPrice(double itemPrice) {
		this.itemPrice = itemPrice;
	}
	public int getItemCategoryId() {
		return itemCategoryId;
	}
	public void setItemCategoryId(int itemCategoryId) {
		this.itemCategoryId = itemCategoryId;
	}
	public String getItemType() {
		return itemType;
	}
	public void setItemType(String itemType) {
		this.itemType = itemType;
	}
	public String getItemImageName() {
		return itemImageName;
	}
	public void setItemImageName(String itemImageName) {
		this.itemImageName = itemImageName;
	}
	
	
	
	
	
	
	
	
	

	
	


}