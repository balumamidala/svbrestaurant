package com.svbResturant.request;

import java.util.ArrayList;

public class OrderDetails {

	
	private int customerId;
	private ArrayList<OrderItem> orderItems;
	
	
	
	
	public ArrayList<OrderItem> getOrderItems() {
		return orderItems;
	}
	public void setOrderItems(ArrayList<OrderItem> orderItems) {
		this.orderItems = orderItems;
	}
	public int getCustomerId() {
		return customerId;
	}
	public void setCustomerId(int customerId) {
		this.customerId = customerId;
	}
	
	
	
}
