package com.svbResturant.response;

public class GetItemCategoryResponse {
	
	private String itemCategoryName;
	private String itemCategoryImageName;
	private int itemCategoryId;
	public String getItemCategoryName() {
		return itemCategoryName;
	}
	public void setItemCategoryName(String itemCategoryName) {
		this.itemCategoryName = itemCategoryName;
	}
	public String getItemCategoryImageName() {
		return itemCategoryImageName;
	}
	public void setItemCategoryImageName(String itemCategoryImageName) {
		this.itemCategoryImageName = itemCategoryImageName;
	}
	public int getItemCategoryId() {
		return itemCategoryId;
	}
	public void setItemCategoryId(int itemCategoryId) {
		this.itemCategoryId = itemCategoryId;
	}
	
	
	
	

}
