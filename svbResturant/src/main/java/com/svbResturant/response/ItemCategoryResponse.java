package com.svbResturant.response;

import java.util.ArrayList;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

public class ItemCategoryResponse extends StatusResponse {
	
	
	
	@JsonInclude(Include.NON_NULL)
	private ArrayList<GetItemCategoryResponse> itemCategories;
	
	
	
	public ArrayList<GetItemCategoryResponse> getItemCategories() {
		return itemCategories;
	}
	public void setItemCategories(ArrayList<GetItemCategoryResponse> itemCategories) {
		this.itemCategories = itemCategories;
	}
	
	
	

}
