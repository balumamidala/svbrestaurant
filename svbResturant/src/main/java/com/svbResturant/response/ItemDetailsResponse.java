package com.svbResturant.response;

import java.util.ArrayList;

public class ItemDetailsResponse extends StatusResponse {

	private ArrayList<ItemResponse> itemDetails;

	public ArrayList<ItemResponse> getItemDetails() {
		return itemDetails;
	}

	public void setItemDetails(ArrayList<ItemResponse> finalResult) {
		this.itemDetails = finalResult;
	}

}
