package com.svbResturant.response;

public class ItemResponse {

	private String itemName;
	private String itemDescription;
	private double itemPrice;
	private String itemType;
	private String itemImageName;
	private int itemId;
	
	
	
	
	public int getItemId() {
		return itemId;
	}
	public void setItemId(int itemId) {
		this.itemId = itemId;
	}
	public String getItemName() {
		return itemName;
	}
	public void setItemName(String itemName) {
		this.itemName = itemName;
	}
	public String getItemDescription() {
		return itemDescription;
	}
	public void setItemDescription(String itemDescription) {
		this.itemDescription = itemDescription;
	}
	public double getItemPrice() {
		return itemPrice;
	}
	public void setItemPrice(double itemPrice) {
		this.itemPrice = itemPrice;
	}
	public String getItemType() {
		return itemType;
	}
	public void setItemType(String itemType) {
		this.itemType = itemType;
	}
	public String getItemImageName() {
		return itemImageName;
	}
	public void setItemImageName(String itemImageName) {
		this.itemImageName = itemImageName;
	}
	
	
	
}
