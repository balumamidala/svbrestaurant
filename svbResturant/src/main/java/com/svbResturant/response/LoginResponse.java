package com.svbResturant.response;

public class LoginResponse extends StatusResponse{
	
	private int customerId;

	public int getCustomerId() {
		return customerId;
	}

	public void setCustomerId(int customerId) {
		this.customerId = customerId;
	}
	

}
