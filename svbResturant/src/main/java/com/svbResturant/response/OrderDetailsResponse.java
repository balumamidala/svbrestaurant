package com.svbResturant.response;

import java.util.ArrayList;

public class OrderDetailsResponse extends StatusResponse {

	private int totalOrders;
	private int customerId;
	private ArrayList<OrderResponse> orderDetails;
	

	public int getCustomerId() {
		return customerId;
	}

	public void setCustomerId(int customerId) {
		this.customerId = customerId;
	}

	public int getTotalOrders() {
		return totalOrders;
	}

	public void setTotalOrders(int totalOrders) {
		this.totalOrders = totalOrders;
	}

	public ArrayList<OrderResponse> getOrderDetails() {
		return orderDetails;
	}

	public void setOrderDetails(ArrayList<OrderResponse> itemDetails) {
		this.orderDetails = itemDetails;
	}

}
