package com.svbResturant.response;

import java.util.ArrayList;

public class OrderResponse {

	private int orderId;
	private ArrayList<OrderItems> orders;
	public int getOrderId() {
		return orderId;
	}
	public void setOrderId(int orderId) {
		this.orderId = orderId;
	}
	public ArrayList<OrderItems> getOrders() {
		return orders;
	}
	public void setOrders(ArrayList<OrderItems> orderItems) {
		this.orders = orderItems;
	}
	
	
	
	

}
